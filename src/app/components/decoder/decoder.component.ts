import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { decodeString } from "pm-encoder";

@Component({
  selector: 'app-decoder',
  templateUrl: './decoder.component.html',
  styleUrls: ['./decoder.component.scss']
})
export class DecoderComponent implements OnInit {

  encrypedText: string;
  key: string;
  decodedText: string;
  constructor(private _snackBar: MatSnackBar) {
    this.encrypedText = '';
    this.key = '';
    this.decodedText = '';
  }

  ngOnInit() {
  }

  onFileLoad() {
    const inputNode: any = document.querySelector('#fileCryped');

    if (typeof (FileReader) !== 'undefined') {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.encrypedText = e.target.result;
        console.log(this.encrypedText);
      };

      reader.readAsText(inputNode.files[0]);
    }
  }


  copy(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    this._snackBar.open('Texto copiado', 'Ok!', {
      duration: 5000,
    });

  }

  createFile() {
    const element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(this.decodedText));
    element.setAttribute('download', '' + new Date().getTime() + '.txt');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }


  decode() {


    this.decodedText = decodeString(this.encrypedText, this.key);
  }

  reset() {
    this.key = '';
    this.decodedText = '';
    this.encrypedText = '';
  }



}

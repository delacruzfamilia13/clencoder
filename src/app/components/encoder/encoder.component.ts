import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatDialog } from "@angular/material/dialog";
import { InfoDialogComponent } from "../Dialogs/info-dialog/info-dialog.component";
import { encodeString } from "pm-encoder";
@Component({
  selector: "app-encoder",
  templateUrl: "./encoder.component.html",
  styleUrls: ["./encoder.component.scss"]
})
export class EncoderComponent implements OnInit {
  key: string;
  textOriginal: string;
  textEncoded: string;
  constructor(private _snackBar: MatSnackBar, private dialog: MatDialog) {
    this.key = "";
    this.textOriginal = "";
    this.textEncoded = "";
  }

  ngOnInit() {}

  generateKey() {
    const randomKey = [...Array(100)]
      .map(i => (~~(Math.random() * 36)).toString(36))
      .join("");
    this.key = randomKey;
  }

  copy(val: string) {
    const selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);

    this._snackBar.open("Texto copiado", "Ok!", {
      duration: 5000
    });
  }

  encode() {
    this.textEncoded = encodeString(this.textOriginal, this.key);
  }

  createFile() {
    const element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/plain;charset=utf-8," + encodeURIComponent(this.textEncoded)
    );
    element.setAttribute("download", "" + new Date().getTime() + ".crypted");

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  reset() {
    this.key = "";
    this.textOriginal = "";
    this.textEncoded = "";
  }

  openInfoKey() {
    this.dialog.open(InfoDialogComponent, {
      width: "400px",
      data: {
        title: "¿Qué papel juega la clave de cifrado?",
        info:
          "Una clave de cifrado es un conjunto de caracteres que sirven para cifrar y posteriormente descifrar cierta información. La misma debe ser secreta, y solamente conocida por el emisor y el/los destinatarios del mensaje, ya que cualquier persona que tenga posesión  de esta clave, podrá descifrar el mensaje."
      }
    });
  }
}

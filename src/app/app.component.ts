import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { InfoDialogComponent } from "./components/Dialogs/info-dialog/info-dialog.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "CLEncoder";
  screen: number;
  constructor(private dialog: MatDialog) {
    this.screen = 1;
  }

  ngOnInit() {
    this.dialog.open(InfoDialogComponent, {
      width: "400",
      data: {
        title: "Bienvenido a CLEncoder",
        info:
          "A continuación podrás facilmente cifrar y descifrar tu información secreta y/o delicada," +
          "para ser distribuida con su destinatario de manera segura y confiable." +
          "Con tres sencillos pasos podrás lograrlo."
      }
    });
  }

  openPrivacity() {
    this.dialog.open(InfoDialogComponent, {
      width: "350px",
      data: {
        title: "Protegemos tu privacidad",
        info:
          "Esta web no está conectada con bases de datos, ni almacena ningún tipo de información," +
          "como cookies, etc. <br> Por lo que tanto su clave de cifrado, su información o su identidad nos" +
          " es completamente ajena. <br> Puede asegurarse usted mismo leyendo o reutilizando el " +
          ' <a target="_blank" href="https://gitlab.com/delacruzfamilia13/clencoder">Código fuente del proyecto</a>. '
      }
    });
  }
}
